package com.bekas.SBweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing

public class SBwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SBwebApplication.class, args);
	}

}
